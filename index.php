<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
    <title>Style Guide Boilerplate</title>
    <meta name="viewport" content="width=device-width">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700|Roboto:400,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet/less" type="text/css" href="css/site.less" />
    <!-- Optional but tasty -->
    <link rel="stylesheet/less" type="text/css" href="css/pine.less" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">  
    <script type="text/javascript">
        less = {
            env: "development", // or "production"
            async: false,       // load imports async
            fileAsync: false,   // load imports async when in a page under
                                // a file protocol
            poll: 1000,         // when in watch mode, time in ms between polls
            functions: {},      // user functions, keyed by name
            dumpLineNumbers: "comments", // or "mediaQuery" or "all"
            relativeUrls: false,// whether to adjust url's to be relative
                                // if false, url's are already relative to the
                                // entry less file
        };
    </script>
    <script src="js/less.js" type="text/javascript"></script>
  </head>
  <body>

    <header>
      <div class="wrapper">
        <div class="row">
          <div class="span6">
            <h1>Fluid Grid Starter Kit</h1>
          </div>
          <nav class="span6">
            <i class="fa fa-bars"></i>
            <ul>
              <li><a href="#typo">Type and Colour</a></li>
              <li><a href="#lists">Lists</a></li>
              <li><a href="#links">Links</a></li>
              <li><a href="#images">Images</a></li>
              <li><a href="#text-mark-up">Text mark up</a></li>
              <li><a href="#forms">Forms</a></li>
              <li><a href="#tables">Tables</a></li>
              <li><a href="#chars">Characters</a></li>  
              <li><a href="#hypenation">Hypenation</a></li>  
            </ul>
          </nav>
        </div>
      </div>
    </header>

    <main id="typo" class="typography-about">
      <div class="wrapper">
        <div class="row about">
          <article class="span9">
            <h2>About</h2>
            <p>This project uses: <strong>HTML5</strong>, <strong>LESS CSS</strong> and the concept of <strong>fluid grids</strong>.</p>
            <p>The point of this project is to give you a selection of HTML5 tags so that you can style them create a style guide for all elements. It is also a fluid grid system that you can drop into your sites and a some useful LESS CSS mixins and variable naming to get you building quickly. It's aimed at starting you off on good footing.</p>
            <p>It's recommended that you take a 2 minute look around to familier yourself with the HTML so you can spot where the section level tags are such as: <code>&lt;main&gt;</code>.</p>
          </article>
        </div>
        <div class="row">
          <div class="span4">
            <h2>Typography</h2>
          </div>
        </div>
        <div class="row">
          <div class="span6">
            <h1>This is a 1st level heading</h1>
            <p>This is a test paragraph.</p>
            <h2>This is 2nd level heading</h2>
            <p>This is a test paragraph.</p>
            <h3>This is 3rd level heading</h3>
            <p>This is a test paragraph.</p>
            <h4>This is 4th level heading</h4>
            <p>This is a test paragraph.</p>
            <h5>This is 5th level heading</h5>
            <p>This is a test paragraph.</p>
            <h6>This is 6th level heading</h6>
            <p>This is a test paragraph.</p>
          </div>
          <div class="span6 colours">
            <h2>Colour Palette</h2>
            <p>The core colour palette as found in the variables.less file.</p>
            <ul>
               <li class="one">Colour Primary  <span>Forrest Green #00940a</span></li>
               <li class="two">Colour Secondary <span>Water blue #0095db</span></li>
               <li class="three">Colour Tertiary <span>Pine needles #cba354</span></li>
               <li class="four">Colour Off Black <span>Copy colour #33333</span></li>
               <li class="five">Colour Grey <span>Ash #fafafa</span></li>
               <li class="six">Colour Link <span>Pine cones #7a5000</span></li> 
            </ul>
          </div>
        </div>
      </div>
    </main>

    <section id="lists" class="lists">
      <div class="wrapper">
        <div class="row">
          <div class="span12">
          <h2>Lists</h2>
            <p>Here we have three lists that consist of the <code>&lt;ul&gt;</code> an unordered list, <code>&lt;ol&gt;</code> an ordered list and <code>&lt;dl&gt;</code> a description list. You can style these to show how you're standard lists will look.</p>
          </div>
        </div>
        <div class="row">
          <div class="span3">
          <h3>Unordered</h3>
            <ul>
              <li> One.</li>
              <li> Two.</li>
              <li> Three. Well, probably this list item should be longer. Note that
            for short items lists look better if they are compactly presented,
                   whereas for long items, it would be better to have more vertical spacing between items.</li>
              <li> Four. This is the last item in this list.
                   Let us terminate the list now without making any more fuss about it.</li>
            </ul>
          </div>
          <div class="span3">
            <h3>Ordered</h3>
            <ol>
              <li> One.</li>
              <li> Two.</li>
              <li> Three. Well, probably this list item should be longer. Note that if
            items are short, lists look better if they are compactly presented,
                   whereas for long items, it would be better to have more vertical spacing between items.</li>
              <li> Four. This is the last item in this list.
                   Let us terminate the list now without making any more fuss about it.</li>
            </ol>
          </div>
          <div class="span3">
            <h3>Descripiton</h3>
            <dl class="coffee">
              <dt><i class="fa fa-coffee"></i> Espresso</dt>
              <dd>Espresso is a strong black coffee</dd>
              <dt><i class="fa fa-coffee"></i> Cappuccino</dt>
              <dd>A true cappuccino is a combination of equal parts espresso, steamed milk and milk froth</dd>
              <dt><i class="fa fa-coffee"></i> Americano</dt>
              <dd>An Americano is a single shot of espresso added to a cup of hot water</dd>
            </dl>
          </div>
        </div>
      </div>
    </section>

    <section id="links" class="wrapper links">
      <div class="row">
        <div class="span8">
          <h2>Links</h2>
          <ul>
            <li> <a href="../index.html">This page</a> </li>
            <li> <a href="http://twitter.com/jimmymorrisuk"
            title="JimmyMorrisUK on Twitter" target="_blank"
            >Follow the creator, JimmyMorrisUK on Twitter</a> </li>
          </ul>
          <p> This is paragraph of text that contains some <a href="/">links</a>. It's here to give you and idea of <a href="">what you're links might look like</a> in the middle of some copy.</p>
        </div>
      </div>
    </section>

    <section id="images" class="images">
      <div class="wrapper">
        <div class="row">
          <div class="span12">
            <h2>Images</h2>
            <p>That's the thing about pictures: they seduce you.</p>
          </div>
        </div>
      </div>
      <figure class="image-text">
        <img src="../images/pine.jpg" alt="Pine forrest" />
        <figcaption><i class="fa fa-camera"></i> Time to go for a walk?</figcaption>
      </figure>
    </section>  


    <section id="text-mark-up" class="wrapper mark-up">
      <div class="row">
        <div class="span9">
          <h2>Text-level markup</h2>
          <ul>
            <li> <abbr title="Cascading Style Sheets">CSS</abbr> (an abbreviation;
           <code>abbr</code> markup used) </li>
            <li> <abbr>Adj. - An abbreviation for adjective.</abbr></li>
            <li> <b>bolded</b> (<code>b</code> markup used - just bolding with unspecified
                 semantics)</li>
            <li> <cite>Origin of Species</cite> (a book title;
                 <code>cite</code> markup used)</li>
            <li> <code>a[i] = b[i] + c[i);</code> (computer code; <code>code</code> markup used)</li>
            <li> here we have some <del>deleted</del> text (<code>del</code> markup used)</li>
            <li> an <dfn>octet</dfn> is an entity consisting of eight bits
                 (<code>dfn</code> markup used for the term being defined)</li>
            <li> this is <em>very</em> simple (<code>em</code> markup used for emphasizing
                 a word)</li>
            <li> <i lang="la">Homo sapiens</i> (should appear in italics;  <code>i</code> markup used)</li>
            <li> here we have some <ins>inserted</ins> text (<code>ins</code> markup used)</li>
            <li> type <kbd>yes</kbd> when prompted for an answer (<code>kbd</code> markup
                 used for text indicating keyboard input)</li>
            <li> <q>Hello!</q> (<code>q</code> markup used for quotation)</li>
            <li> He said: <q>She said <q>Hello!</q></q> (a quotation inside a quotation)</li>
            <li> you may get the message <samp>Core dumped</samp> at times
                 (<code>samp</code> markup used for sample output)</li>
            <li> <small>this is not that important</small> (<code>small</code> markup used)</li>
            <li> <strong>this is highlighted text</strong> (<code>strong</code>
                 markup used)</li>
            <li> In order to test how subscripts and superscripts (<code>sub</code> and
                 <code>sup</code> markup) work inside running text, we need some
                 dummy text around constructs like x<sub>1</sub> and H<sub>2</sub>O
                 (where subscripts occur). So here is some fill so that
                 you will (hopefully) see whether and how badly the subscripts
                 and superscripts mess up vertical spacing between lines.
                 Now superscripts: M<sup>lle</sup>, 1<sup>st</sup>, and then some
                 mathematical notations: e<sup>x</sup>, sin<sup>2</sup> <i>x</i>,
                 and some nested superscripts (exponents) too:
                 e<sup>x<sup>2</sup></sup> and f(x)<sup>g(x)<sup>a+b+c</sup></sup>
                 (where 2 and a+b+c should appear as exponents of exponents).</li>
            <li> <u>underlined</u> text (<code>u</code> markup used)</li>
            <li> the command <code>cat</code> <var>filename</var> displays the
                 file specified by the <var>filename</var> (<code>var</code> markup
                 used to indicate a word as a variable).</li>
            <li> <data value="2000">Lots of sugar.</data> The <code>data</code> tag. </li>
            <li> <time>20:35</time> A great <code>time</code> to meet. </li>
            <li> <mark>Highlighted</mark> using the <code>mark</code> tag.</li>
            <li>  Here we have an example of the <code>address</code> tag, great for semantics. 
                  <address>
                    A name,
                    A street,
                    A city,
                    A postcode 
                  </address> 
            </li>
            <li>
                A <code>blockquote</code> example in action.
                <blockquote>Do not dwell in the past, do not dream of the future, concentrate the mind on the present moment.</blockquote>
            </li>
          </ul>

          <p>Some of the elements tested above are typically displayed in a monospace
          font, often using the <em>same</em> presentation for all of them. This
          tests whether that is the case on your browser:</p>

          <ul>
            <li> <code>This is sample text inside code markup</code></li>
            <li> <kbd>This is sample text inside kbd markup</kbd></li>
            <li> <samp>This is sample text inside samp markup</samp></li>
          </ul>
        </div>
        <aside class="span3">
          <img src="../images/pine-snow.jpg" alt="Pine trees in the snow." />
        </aside>
      </div>
    </section>


    <section id="forms" class="wrapper forms">
      <div class="row">
        <div class="span6">

          <h2>Forms</h2>

          <form action="http://www.cs.tut.fi/cgi-bin/run/~jkorpela/echo.cgi">
          <div>
          <input type="hidden" name="hidden field" value="42">
          This is a form containing various fields (with some initial
          values (defaults) set, so that you can see how input text looks
          like without actually typing it):</div>
          <div><label for="but">Button:
          <button id="but" type="submit" name="foo" value="bar">A cool button</button></label></div>
          <div><label for="f0">Reset button:
          <input id="f0" type="reset" name="reset" value="Reset"></label></div>
          <div><label for="f1">Single-line text input field: <input id="f1" type="text" name="text" size="20" value="Default text."></label></div>
          <div><label for="f2">Multi-line text input field (textarea):</label>
          <textarea id="f2" name="textarea" rows="2" cols="20">Default text.
          </textarea></div>
          <div>The following two radio buttons are inside
          a <code>fieldset</code> element with a <code>legend</code>:</div>
          <fieldset>
          <legend>Legend</legend>
          <div><label for="f3"><input id="f3" type="radio" name="radio" value="1"> Radio button 1</label></div>
          <div><label for="f4"><input id="f4" type="radio" name="radio" value="2" checked> Radio button 2 (initially checked)</label></div>
          </fieldset>
          <fieldset>
          <legend>Check those that apply</legend>
          <div><label for="f5"><input id="f5" type="checkbox" name="checkbox"> Checkbox 1</label></div>
          <div><label for="f6"><input id="f6" type="checkbox" name="checkbox2" checked> Checkbox 2 (initially checked)</label></div>
          </fieldset>
          <div><label for="f10">A <code>select</code> element with <code>size="1"</code>
          (dropdown box):
          <select id="f10" name="select1" size="1">
          <option>one
          <option selected>two (default)
          <option>three
          </select></label></div>
          <div><label for="f11">A <code>select</code> element with <code>size="3"</code>
          (listbox):</label>
          <select id="f11" name="select2" size="3">
          <option>one
          <option selected>two (default)
          <option>three
          </select></div>
          <div><label for="f99">Submit button:
          <input id="f99" type="submit" name="submit" value="Just a test"></label></div>
          </form>
        </div>
      </div>
    </section>

    <section id="tables" class="wrapper tables">
      <div class="row">
        <div class="span7">

          <h2>Tables</h2>

          <p>The following table has a caption. The first row and the first column
          contain table header cells (<code>th</code> elements) only; other cells
          are data cells (<code>td</code> elements), with <code>align="right"</code>
          attributes:</p>

          <table>
            <caption>Sample table: areas of the Nordic countries, in sq km</caption>
            <tr><th scope="col">Country</th> <th scope="col">total area</th> <th scope="col">land area</th>
            <tr><th scope="row">Denmark</th> <td> 43,070 </td><td> 42,370</tr>
            <tr><th scope="row">Finland</th> <td>337,030 </td><td>305,470</tr>
            <tr><th scope="row">Iceland</th> <td>103,000 </td><td>100,250</tr>
            <tr><th scope="row">Norway</th>  <td>324,220 </td><td>307,860</tr>
            <tr><th scope="row">Sweden</th>  <td>449,964 </td><td>410,928</tr>
          </table>
        </div>
      </div>
    </section>

    <section id="chars" class="wrapper character">
      <div class="row">
        <div class="span7">
          <h2>Character test</h2>
          <p>The following table has some sample characters with
          annotations. If the browser&#8217;s default font does not
          contain all of them, they may get displayed using backup fonts.
          This may cause stylistic differences, but it should not
          prevent the characters from being displayed at all.</p>

          <table>
            <tr><th>Char. <th>Explanation <th>Notes
            <tr><td>ê <td>e with circumflex <td>Latin 1 character, should be ok
            <tr><td>&#8212; <td>em dash <td>Windows Latin 1 character, should be ok, too
            <tr><td>&#x100; <td>A with macron (line above) <td>Latin Extended-A character, not present in all fonts
            <tr><td>&Omega; <td>capital omega <td>A Greek letter
            <tr><td>&#x2212; <td>minus sign <td>Unicode minus
            <tr><td>&#x2300; <td>diameter sign <td>relatively rare in fonts
          </table>

        </div>
      </div>
    </section>

    <section id="hypenation" class="wrapper character">
      <div class="row">
        <div class="span5">

          <h2>Hyphenation</h2>
          <p>In the following, a width setting should cause some hyphenation,
          depending on support to various methods of hyphenation.</p>

          <h4>CSS-based hyphenation</h4>
          <p class="limited hyphens">Until recently the great majority of naturalists believed that species were immutable productions, and had been separately created. This view has been ably maintained by many authors.

          <h4>Explicit hyphenation hints (soft hyphens)</h4>
          <p class="limited">Un­til re­cent­ly the great
           ma­jor­i­ty of nat­u­ral­ists
          be­lieved that spe­cies were
           im­mu­ta­ble
           pro­duc­tions,
           and had been sep­a­rate­ly cre­at­ed.
          This view has been ably main­tain­ed by many au­thors.
        </div>
      </div>
    </section>

    <footer>
      <div class="row">
        <div class="span4">
          <h3>Created by James Morris</h3>
          <small>HTML and copy base on work by Jukka Korpela</small>
        </div>
        <div class="span6 offset2">
      
        </div>
      </div>
    </footer>

  </body>
</html>